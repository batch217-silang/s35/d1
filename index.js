const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001


/*[SECTION] MONGODB CONNECTION
SYNTAX: mongoose.connect("SRV LINK", {useNewUrlParser: true, useUnifiedTopology:true})
*/

mongoose.connect("mongodb+srv://admin:admin123@zuitt.25fvemh.mongodb.net/B217_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"))


/******************************************************/
/*[SECTION] MONGOOSE SCHEMAS
It determines the structure of our documents to be stored in the database. It acts as our data blueprint and guide
*/
const taskSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [true, "Task name is required"]
	},
	status: {
		type: String,
		default: "pending"
	}
});


// USER SCHEMA
const userSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [true, "New user is required"]	
	},
	password: {
		type: String,
		required: [true, "New user is required"]
	}
});


/******************************************************/
// [SECTION] MODELS = uses the schema
const Task = mongoose.model("Task", taskSchema);

// USER MODEL
const User = mongoose.model("User", userSchema);


app.use(express.json());
app.use(express.urlencoded({extended:true}));


/******************************************************/
// CREATING A NEW TASK // BUSINESS LOGIC
app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) =>{
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate found");
		}else{
			let newTask= new Task({
				name: req.body.name
			});

			newTask.save((saveErr, saveTask) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New task created.");
				}
			})
		}
	})
})


// USER POST 
app.post("/signup", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) =>{
		if(result != null && result.name == req.body.name && result.password == req.body.password){
			return res.send("Duplicate user found");
		}else{
			let newUser= new User({
				name: req.body.name,
				password: req.body.password
			});

			newUser.save((saveErr, saveTask) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New user registered.");
				}
			})
		}
	})
})


/******************************************************/
// GET METHOD
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
	})
})


// [SECTION] CREATING OF TO DO LIST ROUTES
// urlencoded = allows your app to read data from forms
app.listen(port, () => console.log(`Server running at port ${port}`));